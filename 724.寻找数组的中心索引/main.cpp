/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月08日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        int size = nums.size();
        int res = -1;
        
        int sum = 0;
        vector<int> left(size, 0);
        vector<int> right(size, 0);
        for(int i = 0; i < size; i++){
            left[i] = sum;
            sum += nums[i];
        }

        sum = 0;
        for(int i = size - 1; i >= 0; i--){
            right[i] = sum;
            sum += nums[i];
        }

        for(int i = 0; i < size; i++){
            if(left[i] == right[i]){
                res = i;
                break;
            }
        }

        return res;
    }
};



class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        int size = nums.size();
        int res = -1;
        
        int sum = 0;
        vector<int> left(size, 0);
        for(int i = 0; i < size; i++){
            left[i] = sum;
            sum += nums[i];
        }

        sum = 0;
        for(int i = size - 1; i >= 0; i--){
            left[i] -= sum;
            sum += nums[i];
        }

        for(int i = 0; i < size; i++){
            if(left[i] == 0){
                res = i;
                break;
            }
        }

        return res;
    }
};


class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        int size = nums.size();
        int res = -1;
        
        int sum = 0;
        vector<int> left(size, 0);
        for(int i = 0; i < size; i++){
            left[i] = sum;
            sum += nums[i];
        }

        sum = 0;
        for(int i = size - 1; i >= 0; i--){
            left[i] -= sum;
            sum += nums[i];
        }

        for(int i = 0; i < size; i++){
            if(left[i] == 0){
                res = i;
                break;
            }
        }

        return res;
    }
};

class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        int total = 0;
        for (int num : nums) total += num;
        int sum = 0;
        for (int i = 0; i < nums.size(); sum += nums[i++])
            if (sum * 2 == total - nums[i])
                return i;
        
        return -1;
    }
};
