/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年08月18日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
#include <stack>
using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
private:
    vector<int> res;
    stack<TreeNode *> s;
    int cur_times = 0, maxtimes = 0;
    TreeNode *pre = NULL;

public:
    vector<int> findMode(TreeNode* root) {
        if( root == NULL ){
            return res;
        }

        pre = root;
        inOrder(root);
        return res;
    }

    void inOrder(TreeNode* root){
        if(root == NULL)
            return;

        inOrder(root->left);
        if(pre){
            cur_times = root->val == pre->val? cur_times+1:1;
            if( cur_times == maxtimes ){
                res.push_back(root->val);
            }else if(cur_times > maxtimes){
                res.clear();
                res.push_back(root->val);
                maxtimes = cur_times;
            }

        }

        pre = root;
        inOrder(root->right);
    }
};
