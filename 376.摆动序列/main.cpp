/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年04月07日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
        int up = 1;
        int down = 1;
        int size = nums.size();
        if (size <= 1)
            return size;

        for( int i = 1; i < size; i++ ){
            if(nums[i] > nums[i - 1])
                up = down + 1;
            else if(nums[i] < nums[i - 1])
                down = up + 1;
        
        }

        return max(up, down);
        
    }
};
