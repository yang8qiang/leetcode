/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年07月24日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <stack>
using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* invertTree(TreeNode* root) {
        if( !root )
            return NULL;

        stack<TreeNode *> s;
        s.push(root);
        TreeNode *p = NULL;
        while( !s.empty() ){
            p = s.top();
            s.pop();
            if( p ){
                s.push(p->left);
                s.push(p->right);
                swap(p->left, p->right);
            }
        }

        return root;
    }
};

