/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年12月16日
*   描    述：
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int sumNumbers(TreeNode* root) {
        if( !root )
            return 0;

        return DFS(root, 0);
    }

private:
    int DFS(TreeNode* root, int sum){
        int cursum = sum * 10 + root->val;

        if( !root->left && !root->right ){
            return cursum;
        }
        
        int val = 0;
        if( root->left )
            val += DFS(root->left, cursum);

        if( root->right )
            val += DFS(root->right, cursum);

        return val;
    }
};

