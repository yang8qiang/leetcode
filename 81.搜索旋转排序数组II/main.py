class Solution:
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: bool
        """
        if not nums:
            return False

        low, high = 0, len(nums) - 1

        while low <= high:
            mid = (low + high) // 2
            if target == nums[mid]:
                return True

            # 判断区间是否有序
            # 这里表示 [left, mid - 1] 是有序的
            if nums[low] == nums[mid]:
                low += 1
            elif nums[low] < nums[mid]:
                if nums[low] <= target <= nums[mid]:
                    high = mid  # 重新划分右边界
                else:
                    low = mid + 1  # target 落在 [mid + 1, right] 这个区间
            else:  # nums[0] > nums[mid] 时，表示 [mid + 1, right] 有序
                if nums[mid] <= target <= nums[high]:  # target 落在 [mid + 1, right] 时
                    low = mid
                else:
                    high = mid - 1  # target 落在 [left, mid - 1] 这个区间,重新划分左边界

        return False