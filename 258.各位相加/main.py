#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2021年12月05日
描    述：
Copyright (C) 2021 All rights reserved.
'''


class Solution:
    def toString(self, num: int) -> list():
        numstr = str(num)
        if len(numstr) > 1:
            argnum = 0
            for i in range(len(numstr)):
                argnum += int(numstr[i])
            return self.toString(argnum)

        return num

    def addDigits(self, num: int) -> int:
        return self.toString(num)

class Solution:
    def addDigits(self, num: int) -> int:
        ans = num
        numstr = str(num)
        while len(numstr) > 1:
            ans = 0
            for i in range(len(numstr)):
                ans += int(numstr[i])
            numstr = str(ans)

        return ans
