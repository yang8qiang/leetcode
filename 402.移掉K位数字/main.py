#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年05月04日
描    述：
Copyright (C) 2019 All rights reserved.
'''

class Solution:
    def removeKdigits(self, num: str, k: int) -> str:
        size = len(num)
        if size <= k:
            return "0"

        res = self.helper(num, k)
        size = len(res)
        i = 0
        while i < size and res[i] == 0:
            i = i + 1

        return "0" if i == size else res[i:]

    def helper(self, num: str, k: int) -> str:
        while k > 0:
            length = len(num)
            i = 0;
            while i + 1 < length and num[i] <= num[i+1]:
                i = i + 1;
            

            if i == length -1:
                num = num[0:length-1]
            else:
                num = num[0:i] + num[i+1:]
            k = k - 1;

        return num
        
