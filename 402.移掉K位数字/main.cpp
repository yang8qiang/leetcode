/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年05月04日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    string removeKdigits(string num, int k) {
        string res = "";
        char c;
        
        int i = 0, size = num.size();
        for(i=0; i < size; i++){
            c = num[i];
            while( res.size() && res.back() > c && k ){
                res.pop_back();
                k--;
            }

            if(res.size() || c != '0'){
                res.push_back(c);
            }
        }
        
        while(res.size() && k){
            res.pop_back();
            k--;
        }

        return res.size() ? res : "0";
    }
};