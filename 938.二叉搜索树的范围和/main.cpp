/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年11月21日
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
private:
    int sum = 0;

public:
    int rangeSumBST(TreeNode* root, int low, int high) {
        if(!root) 
            return 0;
        if(root->val < low){
            rangeSumBST(root->right, low, high);
        } else if(root->val > high){
            rangeSumBST(root->left, low, high);
        } else{
            sum += root->val;
            rangeSumBST(root->left, low, high);
            rangeSumBST(root->right, low, high);
        }

        return sum;
    }
};