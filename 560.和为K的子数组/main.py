#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年03月05日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def subarraySum(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """

        dic = dict()
        res = 0
        sum = 0
        dic[0] = 1
        for num in nums:
            sum += num
            val = dic.get(sum-k, 0)
            res += val
            dic[sum] = dic.get(sum, 0) + 1

        return res