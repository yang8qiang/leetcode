#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2021年12月07日
描    述：
Copyright (C) 2021 All rights reserved.
'''


class Solution:
    def dfs(self, n: int):
        for i in range(0, 10):
            m = n * 10 + i
            if m > self.n:
                break
            self.ans.append(m)
            self.dfs(m)

    def lexicalOrder(self, n: int) -> List[int]:
        self.n = n
        self.ans = list()

        for i in range(1, 10):
            if i > self.n:
                break
            self.ans.append(i)
            self.dfs(i)

        return self.ans
