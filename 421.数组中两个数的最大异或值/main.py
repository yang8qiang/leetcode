#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年06月19日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def findMaximumXOR(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        maxbit = 0
        maxres = 0
        for num in nums:
            maxbit = max(maxbit, num.bit_length())

        flag = 0
        for bit in range(maxbit, -1, -1):
            hashs = set()
            flag |= 1 << bit
            for num in nums:
                hashs.add(flag & num)

            tmp = maxres | (1 << bit)

            hashs = tuple(hashs)
            for num in hashs:
                if hashs.count(num ^ tmp):
                    maxres = tmp
                    break

        return maxres
