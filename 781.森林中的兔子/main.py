#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年12月13日
描    述：
'''


class Solution(object):
    def numRabbits(self, answers):
        """
        :type answers: List[int]
        :rtype: int
        """
        answers = sorted(answers)
        ans = 0
        dic = dict()
        for pos, val in enumerate(answers):
            count = dic.get(val, None)
            if count is None:
                dic[val] = val
                ans += val + 1
            elif count == 0:
                ans += val + 1
                dic[val] = val
            else:
                dic[val] -= 1

        return ans
