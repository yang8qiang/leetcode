#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2021年12月05日
描    述：
Copyright (C) 2021 All rights reserved.
'''

class Solution:
    def reverseBits(self, n: int) -> int:
        bits = 32
        ans = 0
        while bits > 0:
            ans <<= 1
            ans += (n & 1)
            n >>= 1
            bits -= 1

        return ans
