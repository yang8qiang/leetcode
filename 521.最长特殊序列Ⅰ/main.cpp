/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月03日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    int findLUSlength(string a, string b) {
        if(a.compare(b) == 0){
            return -1;
        }

        return max(a.size(), b.size());
    }
};