/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年02月19日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    int maxArea(vector<int>& height) {
        int left = 0, right = height.size() - 1;
        int maxarea = 0, tmparea = 0;
        while( left != right ){
            tmparea = min(height[left], height[right]) *(right - left);
            maxarea = max(maxarea, tmparea);
            if( height[left] < height[right] )
                left++;
            else
                right--;
        }

        return maxarea;
    }
};
