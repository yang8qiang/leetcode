#include <iostream>
#include <vector>
#include <stack>
using namespace std;

class Solution
{
public:
    int findTargetSumWays(vector<int> &nums, int target)
    {
        int m = nums.size();
        int sum = 0, diff, neg;
        for (int i = 0; i < m; i++)
        {
            sum += nums[i];
        }
        diff = sum - target;
        if (diff < 0 || diff % 2 != 0)
        {
            return 0;
        }
        neg = diff / 2;

        vector<vector<int>> dp(m + 1, vector<int>(neg + 1, 0));
        dp[0][0] = 1;
        for (int i = 1; i <= m; i++)
        {
            int num = nums[i - 1];
            for (int j = 0; j <= neg; j++)
            {
                dp[i][j] = dp[i - 1][j];
                if (j >= num)
                {
                    dp[i][j] += dp[i - 1][j - num];
                }
            }
        }

        return dp[m][neg];
    }
};
