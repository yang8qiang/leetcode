/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月16日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int maxDistToClosest(vector<int>& seats) {
        int size = seats.size();
        if(size == 0){
            return 0;
        }

        int count_zero = 0;
        int maxres = 0, start = 0;
        if(seats[0] == 0){
            for(start = 0 ; start < size; start++){
                if(seats[start] == 0){
                    count_zero++;
                }else{
                    break;
                }
            }
        }

        maxres = count_zero;

        for(start; start < size; start++){
            if(seats[start] == 1){
                maxres = max(count_zero/2+count_zero%2, maxres);
                count_zero = 0;
            }else{
                count_zero++;
            }
        }

        if(seats[size - 1] == 0){
            return max(count_zero, maxres);
        }

        return maxres;
    }
};



class Solution {
public:
    int maxDistToClosest(vector<int>& seats) {
        int size = seats.size();
        if(size == 0){
            return 0;
        }

        vector<int> left(size, 0);
        vector<int> right(size, 0);
        int resmax = 0;

        if(seats[0] == 0){
            left[0] = 1;
        }
        for(int i = 1; i < size; i++){
            if(seats[i] == 0){
                left[i] = left[i-1] + 1;
            }
        }

        if(seats[size - 1] == 0){
            right[size - 1] = 1;
        }
        for(int j = size - 2; j >= 0; j--){
            if(seats[j] == 0){
                right[j] = right[j+1] + 1;
            }
        }

        for(int i = 1; i < size-1; i++){
            if(seats[i] == 0){
                resmax = max(resmax, min(left[i], right[i]));
            }
        }

        resmax = max(resmax, left[size-1]);
        resmax = max(resmax, right[0]);

        return resmax;
    }
};t