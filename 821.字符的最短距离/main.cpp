#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
    vector<int> shortestToChar(string S, char C) {
        int size = S.size();
        vector<int> res(size, 0);

        int pre = 1 - size;
        for(int i = 0; i < size; i++){
            if(S[i] == C){
                pre = i;
            }

            res[i] = i - pre;
        }

        pre = 2*size;
        for(int i = size - 1; i >= 0; i--){
            if(S[i] == C){
                pre = i;
            }

            res[i] = min(res[i], pre - i);
        }

        return res;
    }
};
