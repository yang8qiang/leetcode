/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年10月20日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int totalHammingDistance(vector<int>& nums) {
        int size = nums.size();
        int res = 0;

        vector<int>count_1(32,0);

        for(int i = 0; i < size; i++){
            int num = nums[i];
            for(int j = 0; j < 32 && num; j++){
                if(num&0x1){
                    count_1[j]++;
                }
                num = num>>1;
            }
        }

        for(int i = 0; i < 32; i++){
            res += (size - count_1[i])*count_1[i];
        }

        return res;
    }
};


class Solution {
public:
    int totalHammingDistance(vector<int>& nums) {
        int size = nums.size();
        int res = 0;

        vector<int>count_1(32,0);

        for(int i = 0; i < 32; i++){
            int bit_count = 0;
            for(int j = 0; j < size; j++){
                if(nums[j] >> i & 0x1){
                    bit_count++;
                }
            }

            res += (size - bit_count)*bit_count;
        }

        return res;
    }
};