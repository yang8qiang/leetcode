/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年11月29日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <stack>
using namespace std;

///**
  //Definition for a binary tree node.
 struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
  };
 ///
class Solution {
public:
    int kthSmallest(TreeNode* root, int k) {
        TreeNode *p = NULL;
        stack<TreeNode *> s;

        p = root;

        while( k != 0 ){
            while( p ){
                s.push(p);
                p = p->left;
            }
        
            p = s.top();
            s.pop();
            if( --k == 0 )
                return p->val;
        
            p = p->right;
        }
        
        return 0;
    }
}; 
