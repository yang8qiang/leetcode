/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年01月05日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int minMoves(vector<int>& nums) {
        int size = nums.size();
        
        int res = 0, minnum = INT_MAX;
        for(int i = 0; i < size; i++){
            minnum = min(minnum, nums[i]);
        }

        for(int i = 0; i < size; i++){
            res += nums[i] - minnum;
        }

        return res;
    }
};