/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年12月21日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    bool validPalindrome(string s) {
        int size = s.length();
        bool res = false;
        int start = -1, end = size;
        while(++start < --end){
            if(s[start] != s[end])
                return helper(s, start, end-1) || helper(s, start+1, end);
        }

        return true;
    }

    bool helper(string &s, int start, int end){
        for(start; start < end; start++,end--){
            if(s[start] != s[end]){
                return false;
            }
        }

        return true;
    }
    
};