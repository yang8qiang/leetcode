/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年12月15日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <queue>
#include <vector>
using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int second_min_num = -1, min_num = -1;
public:
    int findSecondMinimumValue(TreeNode* root) {
        if(!root->left){
            return -1;
        }
        min_num = root->val;

        int left = getSecondMinimumValue(root->left);
        int right = getSecondMinimumValue(root->right);

        if(left == min_num && right == min_num){
            return -1;
        }

        if(left == min_num && right != min_num){
            return right;
        }

        if(right == min_num && left != min_num){
            return left;
        }

        return min(right, left);
    }

    int getSecondMinimumValue(TreeNode* root){
        if(!root){
            return min_num;
        }

        if(root->val > min_num){
            return root->val;
        }

        int left = getSecondMinimumValue(root->left);
        int right = getSecondMinimumValue(root->right);

        if(left == min_num && right != min_num){
            return right;
        }

        if(right == min_num && left != min_num){
            return left;
        }

        return min(left, right);
    }

};class Solution {
private:
    int second_min_num = -1, min_num = -1;
public:
    int findSecondMinimumValue(TreeNode* root) {
        if(!root->left){
            return -1;
        }
        min_num = root->val;

        int left = getSecondMinimumValue(root->left);
        int right = getSecondMinimumValue(root->right);

        if(left == min_num && right == min_num){
            return -1;
        }

        if(left == min_num && right != min_num){
            return right;
        }

        if(right == min_num && left != min_num){
            return left;
        }

        return min(right, left);
    }

    int getSecondMinimumValue(TreeNode* root){
        if(!root){
            return min_num;
        }

        if(root->val > min_num){
            return root->val;
        }

        int left = getSecondMinimumValue(root->left);
        int right = getSecondMinimumValue(root->right);

        if(left == min_num && right != min_num){
            return right;
        }

        if(right == min_num && left != min_num){
            return left;
        }

        return min(left, right);
    }

};