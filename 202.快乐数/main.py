#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年06月23日
描    述：
'''

class Solution:
    def isHappy(self, n: int) -> bool:
        hs = []
        sum_tmp = 0;
        tmp = 0;

        while n != 1:
            if n in hs:
                return False
            else:
                hs.append(n)

            sum_tmp = 0;
            tmp = 0;

            while n != 0:
                tmp = n % 10
                sum_tmp += tmp*tmp
                n = n//10

            n = sum_tmp

        return True
