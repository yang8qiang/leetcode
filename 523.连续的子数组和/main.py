#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年10月07日
版    本：v1.0.0
描    述：
Copyright (C) 2019 All rights reserved.
'''


class Solution(object):
    def checkSubarraySum(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        length = len(nums)
        if length <= 1:
            return False
        for i in range(0, length - 1):
            if nums[i] == 0 and nums[i+1] == 0:
                return True
        if k == 0:
            return False
        if k < 0:
            k = -k

        sum = 0
        hashmap = dict()
        hashmap[0] = -1
        for i in range(0, length):
            sum += nums[i]
            mod = sum % k
            pos = hashmap.get(mod)
            if pos != None:
                if i - pos >= 2:
                    return True
            else:
                hashmap[mod] = i

        return False

        
