/*================================================================
*   文件名称：main.cpp
*   创建日期：2020年10月28日
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution{
public:
    int peakIndexInMountainArray(vector<int> &arr){
        int size = arr.size();
        int left = 0, right = size - 1;
        int mid;
        while(left < right){
            mid = (right - left)/2 + left;
            if(arr[mid] > arr[mid+1]){
                right = mid;
            }else{
                left = mid + 1;
            }
        }

        return left;
    }
};