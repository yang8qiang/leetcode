#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年05月11日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''

class Solution(object):
    def maxProfitAssignment(self, difficulty, profit, workers):
        """
        :type difficulty: List[int]
        :type profit: List[int]
        :type worker: List[int]
        :rtype: int
        """
        length = len(difficulty)

        jobs = sorted(zip(difficulty, profit))
        workers = sorted(workers)
        
        maxres = i = best = 0
        for work in workers:
            while i < length and work >= jobs[i][0]:
                best = max(best, jobs[i][1])
                i += 1

            maxres += best
            
        return maxres
            


