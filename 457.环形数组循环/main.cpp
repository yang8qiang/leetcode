/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年05月16日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
using namespace std;


class Solution {
public:
    bool circularArrayLoop(vector<int>& nums) {
       int size = nums.size();
       if( size <= 1 ){
           return false;
       }

       bool flag = false;
       int step = 0;
       for(int i = 0; i < size; i++){
           if( nums[i] == 0 )
               continue;
           step = 1;

           if(nums[i] > 0){
                int j = (i + nums[i])%size;
                while( j != i && step <= size){
                    if(nums[j] <= 0){
                        break;
                    }else{
                        j = (j + nums[j])%size;

                        step++;
                    }
                }
                if(j == i && step <= size && step >= 2){
                    flag = true;
                    break;
                }

           }else{
                int j = (( i + nums[i])%size + size)%size;
                while( j != i && step <= size){
                    if(nums[j] >= 0){
                        break;
                    }else{
                        j = (( j + nums[j])%size + size)%size;
                        step++;
                    }
                }
                if(j == i && step <= size && step >= 2){
                    flag = true;
                    break;
                }

           }
       }

       return flag;
    }
};
