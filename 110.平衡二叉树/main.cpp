/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年03月06日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isBalanced(TreeNode* root) {
        if( !root )
            return true;
        int deep = getdeep(root);
        if(deep == -1)
            return false;

        return true;

    }

private:
    int getdeep(TreeNode* root){
        if( root == NULL )
            return 0;

        int leftdeep = getdeep(root->left);
        if( leetdeep == -1 )
            return -1;

        int rightdeep = getdeep(root->right);
        if( rightdeep == -1 )
            return -1;

        if(abs(rightdeep - leftdeep) > 1)
            return -1;

        return 1 + max(leftdeep, rightdeep);
    }
};
