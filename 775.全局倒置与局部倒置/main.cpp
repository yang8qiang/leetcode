/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年11月29日
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    bool isIdealPermutation(vector<int>& A) {
        int size = A.size();
        if(size == 1 || size == 2){
            return true;
        }

        for(int i = 0; i < size - 2; i++){
            for(int j = i + 2; j < size; j++){
                if(A[i] > A[j]){
                    return false;
                }
            }
        }

        return true;
    }
};


class Solution {
public:
    bool isIdealPermutation(vector<int>& A) {
        int size = A.size();
        if(size == 1 || size == 2){
            return true;
        }

        int floor = A[size - 1];
        for(int i = size - 1; i >= 2; i--){
            floor = min(A[i], floor);
            if(A[i-2] > floor){
                return false;
            }
        }

        return true;
    }
};