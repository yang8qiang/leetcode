/*================================================================
*   文件名称：main.c
*   创 建 者：yang qiang
*   创建日期：2018年07月22日
*   描    述：
*   
================================================================*/

class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        int low = 0, high = nums.size() - 1;
        if( high == 0 )
            return 0;
        int mid1 = 0, mid2 = 0;
        while( low < high ){
            mid1 = low + (high - low)/2;
            mid2 = mid1 + 1;    
            if( nums[mid1] < nums[mid2] )
                low = mid2;
            else
                high = mid1;
        }
        return low;
    }
};

