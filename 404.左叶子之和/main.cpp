#include <iostream>
#include <stack>

using namespace std;

/* Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */


class Solution {
private:
    int get_sum_of_left_leves(TreeNode* root){
        if(!root){
            return 0;
        }

        if(root->left&&root->left->left==NULL && root->left->right==NULL){
            return get_sum_of_left_leves(root->right)+root->left->val;
        }

        return get_sum_of_left_leves(root->left) + get_sum_of_left_leves(root->right);
    }

public:
    int sumOfLeftLeaves(TreeNode* root) {
        return get_sum_of_left_leves(root);
    }
};