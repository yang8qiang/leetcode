/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年12月15日
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    char nextGreatestLetter(vector<char>& letters, char target) {
        int size = letters.size();
        int left = 0, right = size - 1;
        int mid;
        while(left <= right){
            mid = (right - left)/2 +left;
            if(letters[mid] <= target){
                left = mid + 1;
            }else if(letters[mid] > target) {
                right = mid-1;
            }
        }

        return letters[left%size];
    }
}; 
