#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int size = nums.size();
        int sum = 0;
        for(int i = 0; i < size; i++){
            sum += nums[i];
        }

        return (size+1) * (size) / 2 - sum;
    }
};

class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int size = nums.size();
        int Xor = 0;
        for(int i = 0; i < size; i++){
            Xor = Xor ^ i ^ nums[i];
        }

        return Xor ^ size;
    }
};