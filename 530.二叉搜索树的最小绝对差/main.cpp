/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年09月26日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <stack>
#include<cmath>
using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int minres = INT_MAX;

public:
    int getMinimumDifference(TreeNode* root) {
        stack<TreeNode* > s;
        TreeNode *curnode = root, *p = NULL,*prenode = NULL;
        while(curnode != NULL || !s.empty()){
            while(curnode){
                s.push(curnode);
                curnode = curnode->left;
            }

            p = s.top();
            s.pop();
            if(prenode != NULL){
                minres = min(minres, abs(p->val - prenode->val));
            }
            prenode = p;
            curnode = p->right;
        }

        return minres;
    }
};