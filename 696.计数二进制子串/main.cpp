/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年12月28日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <string>
using namespace std;


class Solution {
public:
    int countBinarySubstrings(string s) {
        int size = s.size();
        int res = 0, pre = 0, k = 1;
        for(int i = 1; i < size; i++){
            if(s[i] == s[i-1]){
                switch(s[i]){
                    case '0':
                        if(2*pre - i + 1 >= 0 && s[2*pre - i + 1] == '1'){
                            res++;
                        }else{
                            pre = -1;
                        }
                        
                        break;
                    case '1':
                        if(2*pre - i + 1 >= 0 && s[2*pre - i + 1] == '0'){
                            res++;
                        }else{
                            pre = -1;
                        }
                        break;
                }

                continue;
            }

            res++;
            pre = i-1;
        }

        return res;
    }
};

class Solution {
public:
    int countBinarySubstrings(string s) {
        int size = s.size();
        int res = 0, cur = 1, pre = 1;
        for(int i = 1; i < size; i++){
            if(s[i] == s[i-1]){
                cur++;
            }else{
                res = min(pre, cur);
                pre = cur;
                cur = 1;
            }
        }

        return res;
    }
};

class Solution {
public:
    int countBinarySubstrings(string s) {
        int size = s.size();
        int res = 0, cur = 1, pre = 0;
        for(int i = 1; i < size; i++){
            if(s[i] == s[i-1]){
                cur++;
            }else{
                res += min(pre, cur);
                pre = cur;
                cur = 1;
            }
        }

        return res + min(pre, cur);
    }
};