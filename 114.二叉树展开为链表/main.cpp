/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年12月23日
*   描    述：
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void flatten(TreeNode* root) {
        TreeNode *now = root;
        TreeNode *p = NULL;
        while( now ){
            if( now->left ){
                p = now->left;
                while( p->right )
                    p = p->right;

                p->right = now->right;
                now->right = now->left;
                now->left = NULL;
            }

            now = now->right;
        }
    }
};
