/*================================================================
 *   文件名称：main.cpp
 *   创 建 者：yang qiang
 *   创建日期：2019年10月20日
 *   版    本：v1.0.0
 *   描    述：
 *   Copyright (C) 2019 All rights reserved.
 *
 * ================================================================*/

#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int islandPerimeter(vector<vector<int>>& grid){
        int m = grid.size();
        if (!m) {
            return 0;
        }

        int n = grid[0].size(), res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    res += 4;
                    if (i > 0 && grid[i - 1][j] == 1) {
                        res -= 2;
                    }

                    if (j > 0 && grid[i][j - 1] == 1) {
                        res -= 2;
                    }
                }
            }
        }

        return res;
    }
};