/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年09月15日
*   描    述：
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode *head) {
        // write your code here
        if(head == NULL)
            return false;
        ListNode *walker = head;
        ListNode *runner = head;

        while( runner->next != NULL && runner->next->next != NULL ) {
            walker = walker->next;
            runner = runner->next->next;
            if( walker == runner )
                return true;
        }

        return false;        
    }
};
