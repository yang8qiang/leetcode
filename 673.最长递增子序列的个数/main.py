#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2021年01月01日
描    述：
Copyright (C) 2021 All rights reserved.
'''


class Solution(object):
    def findNumberOfLIS(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        size = len(nums)
        length = [1 for _ in range(size)]
        count = [1 for _ in range(size)]

        for j in range(0, size):
            for i in range(j):
                if nums[i] < nums[j]:
                    if length[i] >= length[j]:
                        length[j] = length[i] + 1
                        count[j] = count[i]
                    elif length[i] + 1 == length[j]:
                        count[j] += count[i]

        longest = max(length)
        ans = 0
        for i in range(size):
            if length[i] == longest:
                ans += count[i]

        return ans
