#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2021年12月07日
描    述：
Copyright (C) 2021 All rights reserved.
'''


class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        first = second = 0
        while second < len(nums):
            if nums[second] != 0:
                nums[first] = nums[second]
                first += 1

            second += 1

        while first < len(nums):
            nums[first] = 0
            first += 1
