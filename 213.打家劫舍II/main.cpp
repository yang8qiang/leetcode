/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年05月21日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*
================================================================*/


#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    int rob(vector<int>& nums) {
        int size = nums.size();
        if( size == 0 )
            return 0;
        if(size == 1)
            return nums[0];

        int max1 = 0, max2 = 0;
        int now1 = nums[size-1], now2 = nums[size-2];

        int lastval = 0;
        int tmp;
        for(int i = size-2; i >= 1; i--){
            if(nums[i] + lastval > now1){
                tmp = now1;
                now1= nums[i] + lastval;
                lastval = tmp;
            }else{
                lastval = now1;
            }
        }

        lastval = 0;
        for(int i = size - 3; i >= 0; i--){
            if(nums[i] + lastval > now2){
                tmp = now2;
                now2 = nums[i] + lastval;
                lastval = tmp;
            }else{
                lastval = now2;
            }
        }

        return max(now1, now2);
    }
};
