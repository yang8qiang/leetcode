/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年04月07日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    int firstUniqChar(string s) {
        int arr[26] = {0};
        int len = s.size();
        for(int i = 0; i < len; i++){
            arr[s[i] - 'a'] += 1;
        }

        for(int i = 0; i < len; i++){
            if(arr[s[i] - 'a'] == 1)
                return i;
        }

        return -1;
    }
};
