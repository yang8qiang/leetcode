/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年10月16日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <stack>
#include <vector>

using namespace std;

class Solution {
vector<int> data;
public:
    bool find132pattern(vector<int>& nums) {
        int size = nums.size();
        if(size < 3){ return false;}

        int min = INT_MIN, tmp = 0;
        for(int i = size - 1; i >= 0; i--){
            tmp = nums[i];
            if(tmp < min){
                return true;
            }

            while(!data.empty() && tmp > data[data.size() - 1]){
                min = data[data.size() - 1];
                data.pop_back();
            }

            data.push_back(tmp);
        }

        return false;
    }
};