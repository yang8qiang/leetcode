#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年02月20日
描    述：
Copyright (C) 2019 All rights reserved.
'''
class Solution(object):
    def canJump(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        length = len(nums)
        i = length - 2
        while i >= 0:
            if nums[i] != 0:
                i = i - 1
                continue
            j = i - 1
            flag = False
            while j >= 0:
                if i - j < nums[j]:
		    i = j
                    flag = True
                    break
                j = j - 1
            if not flag:
                return False
            
        return True
            
