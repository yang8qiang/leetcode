/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年01月01日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
public:
    void reorderList(ListNode* head) {
        if( !head || !head->next )
            return ;

        ListNode *p1 = head;
        ListNode *p2 = head->next;

        while( p2 && p2->next ){
            p1 = p1->next;
            p2 = p2->next->next;
        }
        
        ListNode *cur = p1->next;
        ListNode *next = cur->next;
        cur->next = NULL;

        while( next ){
            cur = next;
            next = cur->next;
            cur->next = p1->next;
            p1->next = cur;
        }

        p2 = head;
        next = p1->next;
        p1->next = NULL;
        
        while( next ){
            cur = next;
            next = cur->next;

            cur->next = p2->next;
            p2->next = cur;

            p2 = p2->next->next;
        }

        return;
    }
};
