#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年02月18日
描    述：
'''

class Solution:
    def lengthOfLongestSubstring(self, s: 'str') -> 'int':
        maxlen = 0
        tmplen = 0
        index = [ -1 for i in range(0, 129) ]
        length = len(s)
        pre = -1
        for i in range(0, length):
            c = ord(s[i])
            if pre <= index[c]:
                maxlen = max(maxlen, tmplen)
                tmplen = i - index[c]
                pre = index[c]
            else:
                tmplen = tmplen + 1
            index[c] = i

        return max(maxlen, tmplen)
