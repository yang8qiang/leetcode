#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年03月15日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def numMatchingSubseq(self, S, words):
        """
        :type S: str
        :type words: List[str]
        :rtype: int
        """

        strsize = len(S)
        wsize = len(words)

        opsmap = [[] for _ in range(26)]

        for i in range(strsize):
            opsmap[ord(S[i]) - ord('a')].append(i)

        res = 0
        for word in words:
            lastpos = -1
            flag = True
            for w in word:
                i = 0
                while i < len(opsmap[ord(w) - ord('a')]):
                    if opsmap[ord(w) - ord('a')][i] > lastpos:
                        lastpos = opsmap[ord(w) - ord('a')][i]
                        break
                    i += 1

                if i == len(opsmap[ord(w) - ord('a')]):
                    flag = False
                    break
                
            
            if flag:
                res += 1

        return res