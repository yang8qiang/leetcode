/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年11月04日
*   描    述：
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {
        if( !head || k < 0 )
            return NULL;

        ListNode *tail = head;
        ListNode *newhead = head;
        int len = 1;
        while( tail->next ){
            len++;
            tail = tail->next;
        }

        tail->next = head;
        if( k %=len ){
            for( int i = 0; i < len - k; i++ ){
                tail = tail->next;
            }
        }

        newhead = tail->next;
        tail->next = NULL;

        return newhead;
    }
};

