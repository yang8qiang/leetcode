#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年03月19日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def fairCandySwap(self, A, B):
        """
        :type A: List[int]
        :type B: List[int]
        :rtype: List[int]
        """

        sumA = 0
        sumB = 0
        dicB = dict()

        for num in A:
            sumA += num

        for num in B:
            sumB += num
            dicB[num] = 1

        for num in A:
            y = num + (sumB-sumA)/2
            if dicB.get(y, None):
                return [num, y]

def fairCandySwap(self, A, B):
    dif = (sum(A) - sum(B)) / 2
    A = set(A)
    for b in set(B):
        if dif + b in A:
            return [dif + b, b]