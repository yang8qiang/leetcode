#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年06月16日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def twoSum(self, numbers, target):
        """
        :type numbers: List[int]
        :type target: int
        :rtype: List[int]
        """

        start = 0
        end = len(numbers) - 1

        while start < end:
            sum = numbers[start] + numbers[end]
            if sum == target:
                break
            
            if sum > target:
                end -= 1
            else:
                start += 1

        return [start+1, end+1]