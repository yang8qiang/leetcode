#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> sortedSquares(vector<int>& A) {
        int size = A.size();
        vector<int> res;

        int i;
        for(i = 0; i < size; i++){
            if(A[i] >= 0){
                break;
            }
        }

        int left = i - 1, right = i;
        while( left >= 0 || right < size ){
            if( left < 0 ){
                res.push_back(A[right] * A[right]);
                right++;
            }else if( right >= size ) {
                res.push_back(A[left] * A[left]);
                left--;
            }else {
                int tmpl = A[left] * A[left];
                int tmpr = A[right] * A[right];
                if( tmpl > tmpr ){
                    res.push_back(tmpr);
                    right++;
                }else{
                    res.push_back(tmpl);
                    left--;
                }
            }
        }

        return res;
    }
};