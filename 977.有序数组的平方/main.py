#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年10月16日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def sortedSquares(self, A):
        """
        :type A: List[int]
        :rtype: List[int]
        """
        res = []
        for i in A:
            res.append(i * i)

        res.sort()

        return res