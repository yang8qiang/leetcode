/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年10月27日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& A) {
        int size = A.size();
        if(size < 3 ){
            return 0;
        }

        int res = 0;
        vector<int> dp(size, 0);
        for(int i = 2; i < size; i++){
            if(A[i] - A[i-1] == A[i-1]-A[i-2]){
                dp[i] = dp[i-1]+1;
                res += dp[i];   
            }
        }

        return res;
    }
};
