/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年04月02日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
#include <map>
using namespace std;

class Solution {
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k) {
        int len = nums.size();
        if( len == 0 )
            return false;

        map<int, int> tmpmap;
        map<int, int>::iterator it;
        tmpmap[nums[0]] = 0;

        for(int i = 1; i < len; i++){
            it = tmpmap.find(nums[i]);
            if( it != tmpmap.end() ){
                if( i - it->second <= k ){
                    return true;
                }
            }
            
            tmpmap[nums[i]] = i;
        }

        return false;
    }
};
