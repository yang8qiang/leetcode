/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年01月05日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
    int minMoves2(vector<int>& nums) {
        int size = nums.size();
        if(size == 0){
            return 0;
        }

        sort(nums.begin(), nums.end());
        int start = 0, end = size - 1;
        int res = 0;
        while(start < end){
            res += (nums[end] - nums[start]);
            start++;
            end--;
        }

        return res;
    }
};