#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年01月05日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution:
    def minMoves2(self, nums: list) -> int:
        nums.sort()
        length = len(nums)
        if length == 0:
            return 0

        if length % 2 == 1:
            mid = nums[(length-1)//2]
        else:
            mid = (nums[(length-1)//2] + nums[length//2])//2

        res = 0
        for num in nums:
            res += abs(num - mid)

        return res


class Solution:
    def minMoves2(self, nums: List[int]) -> int:
        nums.sort()
        length = len(nums)
        if length == 0:
            return 0

        start = 0
        end = length - 1
        res = 0
        while start < end:
            res += (nums[end] - nums[start])
            start += 1
            end -= 1

        return res
