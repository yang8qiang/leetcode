/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年02月27日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
#include <queue>
using namespace std;

/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Solution {
public:
    vector<vector<int>> levelOrder(Node* root) {
        vector<vector<int>> res;
        if(root == NULL){
            return res;
        }

        queue<Node *> q;
        Node* p = NULL;
        vector<int> tmp;
        q.push(root);
        int len = 0;
        while( !q.empty() ){
            len = q.size();
            while( len > 0 ){
                p = q.front();
                q.pop();
                for(int i = 0; i < (p->children).size(); i++){
                    q.push(p->children[i]);
                }

                tmp.push_back(p->val);
                len--;
            }

            res.push_back(tmp);
            tmp.clear();
        }

        return res;
    }
};