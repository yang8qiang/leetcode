class Solution(object):
    def findMaxLength(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        size = len(nums)
        if size < 2:
            return 0

        sum = 0
        dp = {}
        dp[0] = -1
        res = 0
        for i in range(0, size):
            if nums == 0:
                sum += nums[i]
            else:
                sum -= nums[i]

            value = dp.get(sum)
            if value == None:
                dp[sum] = i
            else:
                res = max(res, i - value)

        return res


class Solution(object):
    def findMaxLength(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        size = len(nums)
        if size < 2:
            return 0

        sum = 0
        dp = {}
        dp[0] = -1
        res = 0
        for i in range(0, size):
            if nums[i] == 0:
                sum -= 1
            else:
                sum += 1

            value = dp.get(sum)
            if value == None:
                dp[sum] = i
            else:
                res = max(res, i - value)

        return res
