/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年01月06日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <math>
using namespace std;

class Solution {
public:
    int trailingZeroes(int n) {
        int res = 0;
        for( int i = 5; n/i > 0; i = i*5 ){
            res += n/i;
        }

        return res;
    }
};

