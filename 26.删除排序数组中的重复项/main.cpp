#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {              
        int len = nums.size();
        if( len == 0 )
            return 0;
        
        int i = 0, j = 1;
        for( j; j < len; j++){
            if( nums[i] != nums[j] )
                nums[++i] = nums[j];
        }

        return i + 1;
    }
};

