/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年09月16日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <deque>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int findBottomLeftValue(TreeNode* root) {
        if(!root){
            return 0;
        }

        int left_value, len=0;
        TreeNode *p = NULL;
        deque<TreeNode *> q;
        q.push_back(root);
        while (!q.empty()){
            len = q.size();
            while( len > 0){
                p = q.front();
                q.pop_front();
                
                if(p->right){
                    q.push_back(p->right);
                }
                if(p->left){
                    q.push_back(p->left);
                }

                len--;
            }
        }
        
        return p->val;
    }
};