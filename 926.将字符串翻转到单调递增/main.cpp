/*================================================================
*   文件名称：main.cpp
*   创建日期：2020年10月20日
*   描    述：
*   
* ================================================================*/


#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution{
public:
    int minFlipsMonoIncr(string S){
        int size = S.size();
        vector<int> dp(size + 1, 0);
        for(int i = 1; i <= size; i++){
            dp[i] = dp[i-1] + (S[i-1] == '1'? 0:1);
            cout<<dp[i]<<endl;
        }

        int res = INT_MAX;
        for(int i = 0; i <= size; i++){
            res = min(res, dp[i] + size - i - (dp[size] - dp[i]));
        }

        return res;
    }
};