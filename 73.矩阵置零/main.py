#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年02月26日
描    述：
Copyright (C) 2019 All rights reserved.
'''

class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        m = len(matrix)
        if m == 0:
            return

        n = len(matrix[0])
        if n == 0:
            return

        row = []
        col = []

        for i in range(0, m):
            for j in range(0, n):
                if matrix[i][j] == 0:
                    row.append(i)
                    col.append(j)

        for i in range(0, m):
            for j in range(0,n)
            if i in row or j in col:
                matrix[i][j] = 0

        return
