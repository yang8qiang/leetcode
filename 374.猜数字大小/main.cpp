/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年04月07日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;
// Forward declaration of guess API.
// @param num, your guess
// @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
int guess(int num);

class Solution {
public:
    int guessNumber(int n) {
        int low = 1;
        int hight = n;
        int mid = 0;
        while( low < hight ){
            mid = (hight - low)/2 +low;
            if(guess(mid) == 0){
                return mid;
            }else if(guess(mid) == -1){
                hight = mid - 1;
            }else{
                low = mid + 1;
            }   
        }

        return low;
    }
};
