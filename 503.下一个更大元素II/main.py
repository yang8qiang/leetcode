#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年09月11日
版    本：v1.0.0
描    述：
Copyright (C) 2019 All rights reserved.
'''


class Solution(object):
    def nextGreaterElements(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        length = len(nums)
        stack = list()
        res = [-1 for _ in range(length)]

        for i in range(length - 1, -1, -1):
            stacklen = len(stack)
            while stacklen > 0 and stack[stacklen-1] <= nums[i]:
                stack.remove(stack[stacklen - 1])
                stacklen -= 1

            if stacklen != 0:
                res[i] = stack[stacklen-1]
            else:
                j = 0
                while j != i and nums[j] <= nums[i]:
                    j = j + 1

                res[i] = nums[j] if j != i else -1

            stack.append(nums[i])

        return res
