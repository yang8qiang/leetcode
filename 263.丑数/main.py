#!/usr/bin/env pytho3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年02月15日
描    述：
Copyright (C) 2019 All rights reserved.
'''

class Solution:
    def isUgly(self, num):
        """
        :type num: int
        :rtype: bool
        """
        if num == 0:
            return False

        for i in 2, 3, 5:
            while num%i == 0:
                num = num/i

        return num == 1
