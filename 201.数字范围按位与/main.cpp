/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年04月02日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    int rangeBitwiseAnd(int m, int n) {
        if( m == 0 )
            return m;

        int factor = 1;
        while( m != n ){
            m = m >> 1;
            n = n >> 1;
            factor = factor << 1;
        }

        return m * factor;
    }
};

