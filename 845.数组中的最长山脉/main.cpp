/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年11月25日
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int longestMountain(vector<int>& arr) {
        int size = arr.size();
        vector<int> left(size);
        left[0] = 0;
        for(int i = 1; i < size; i++ ){
            left[i] = arr[i] > arr[i-1] ? left[i-1] + 1 : 0;
        }

        vector<int> right(size);
        right[size - 1] = 0;
        for(int i = size - 2; i >= 0; i--){
            right[i] = arr[i] > arr[i + 1] ? right[i+1] + 1 : 0;
        }

        int ans = 0;
        for(int i = 1; i < size - 1; i++){
            if(left[i] > 0 && right[i] > 0){
                ans = max(ans, left[i] + right[i] + 1);
            }
        }

        return ans;
    }
};
