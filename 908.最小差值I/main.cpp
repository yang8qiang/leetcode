/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年11月16日
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int smallestRangeI(vector<int>& A, int K) {
        int size = A.size();
        int maxA = INT_MIN, minA = INT_MAX;
        for(int i = 0; i < size; i++){
            minA = min(minA, A[i]);
            maxA = max(maxA, A[i]);
        }

        return max(0, maxA - minA - 2*K);
    }
};