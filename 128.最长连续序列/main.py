class Solution(object):
    def longestConsecutive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        hash = dict()

        res = 0
        for num in nums:
            if num not in hash:
                leftmax = hash.get(num - 1, 0)
                rightmax = hash.get(num + 1, 0)
                cur_length = leftmax + rightmax + 1
                if cur_length > res:
                    res = cur_length

                hash[num - leftmax] = cur_length
                hash[num + rightmax] = cur_length
                hash[num] = cur_length

        return res
