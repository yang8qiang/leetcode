class Solution(object):
    def detectCapitalUse(self, word):
        """
        :type word: str
        :rtype: bool
        """
        length = len(word)
        upper = 0
        lower = 0
        for i in range(0, length):
            if word[i].islower():
                lower += 1
            elif word[i].isupper():
                upper += 1
            else:
                return False
        
        if upper == length or lower == length:
            return True
        elif word[0].isupper() and upper == 1:
            return True
        else:
            return False