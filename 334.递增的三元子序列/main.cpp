/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年05月20日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
#include <stack>

using namespace std;

class Solution {
public:
    bool increasingTriplet(vector<int>& nums) {
        int size = nums.size();
        if(size <= 2)
            return false;

        int a = INT_MAX, b = INT_MAX;
        int node = 0;
        for(int i = 0; i < size; i++){
            node = nums[i];
            if( node <= a ){
                a = node;
            }else if( node <= b ){
                b = node;
            }else {
                return true;
            }
        }

        return false;
    }
};
