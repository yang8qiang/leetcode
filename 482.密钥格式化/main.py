#!/usr/bin/env python3
# -*- coding: UTF-8 -*-


class Solution:
    def licenseKeyFormatting(self, s: str, k: int) -> str:
        s = s.upper()
        strs = s.split("-")
        strs = "".join(strs)

        strlen = len(strs)
        firstlen = strlen % k
        if firstlen == 0:
            firstlen = k

        strlist = []
        strlist.append(strs[0:firstlen])
        start = firstlen
        while start < strlen:
            strlist.append(strs[start:start + k])
            start += k
        return "-".join(strlist)
