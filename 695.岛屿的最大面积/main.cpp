/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年12月22日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
private:
    int n=0, m = 0;
    int maxarea = 0 ,area = 0;
public:
    int maxAreaOfIsland(vector<vector<int>>& grid) {
        m = grid.size();
        if(m == 0){
            return 0;
        }

        n = grid[0].size();

        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(grid[i][j] == 1){
                    area = getAreaOfIsland(grid, i, j);
                    maxarea = max(maxarea, area);
                }
            }
        }

        return maxarea;
    }

    int getAreaOfIsland(vector<vector<int>>& grid, int i, int j){
        if(i < 0 || i == m){
            return 0;
        }

        if(j < 0 || j == n){
            return 0;
        }
        
        if(grid[i][j] == 1){
            grid[i][j] = 0;
            return 1 + getAreaOfIsland(grid, i-1, j) + getAreaOfIsland(grid, i+1, j) + getAreaOfIsland(grid, i, j-1) + getAreaOfIsland(grid, i, j+1);
        }

        return 0;
    }
};


class Solution {
private:
    int n=0, m = 0;
    int maxarea = 0 ,arae = 0;
public:
    int maxAreaOfIsland(vector<vector<int>>& grid) {
        m = grid.size();
        if(m == 0){
            return 0;
        }

        n = grid[0].size();

        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(grid[i][j] == 1){
                    arae = 1;
                    grid[i][j] = 0;
                    getAreaOfIsland(grid, i, j);
                    maxarea = max(maxarea, arae);
                }
            }
        }

        return maxarea;
    }

    void getAreaOfIsland(vector<vector<int>>& grid, int i, int j){
        int k;
        for(k = i-1; k>= 0; k--){
            if(grid[k][j] == 1){
                arae++;
                grid[k][j] = 0;
                getAreaOfIsland(grid, k, j);
            }else{
                break;
            }
        }

        for(k = i+1; k < m; k++){
            if(grid[k][j] == 1){
                arae++;
                grid[k][j] = 0;
                getAreaOfIsland(grid, k, j);
            }else{
                break;
            }
        }

        for(k = j-1; k >= 0; k--){
            if(grid[i][k] == 1){
                arae++;
                grid[i][k] = 0;
                getAreaOfIsland(grid, i, k);
            }else{
                break;
            }
        }

        for(k = j+1; k < n; k++){
            if(grid[i][k] == 1){
                arae++;
                grid[i][k] = 0;
                getAreaOfIsland(grid, i, k);
            }else{
                break;
            }
        }
    }
};