/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年12月14日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    bool checkPossibility(vector<int>& nums) {
        int size = nums.size();

        int count = 0;
        for(int i = 0; i < size; i++){
            if(i + 1 < size && nums[i] > nums[i+1]){
                if(count > 1){
                    break;
                }
                count++;

                if(i - 1 >= 0 && nums[i - 1] > nums[i+1]){
                    nums[i+1] = nums[i];
                }else{
                    nums[i] = nums[i+1];
                }
            }
        }

        return count <= 1;
    }
};