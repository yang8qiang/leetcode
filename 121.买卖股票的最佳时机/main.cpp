/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年06月04日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>

using namespace std;


class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int size = prices.size();
        if(size == 0)
            return 0;

        int maxres = 0, minprices = prices[0];

        for(int i = 1; i < size; i++){
            if( prices[i] < minprices ){
                minprices = prices[i];
                continue;
            }

            maxres = max( maxres, prices[i] - minprices );
        }

        return maxres;
    }
};


class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int maxres = 0, minprices = INT_MAX;

        for(int i = 0; i < prices.size(); i++){
            minprices = min(minprices, prices[i]);
            maxres = max( maxres, prices[i] - minprices );
        }

        return maxres;
    }
};
