/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年10月16日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int maxres = 0;
public:
    int diameterOfBinaryTree(TreeNode* root){
        if(!root)
            return 0;
        
        deep(root);
        return maxres;
    }
    
    int deep(TreeNode* root) {
       int left = !root->left? 0:deep(root->left);
       int right = !root->right? 0:deep(root->right);

       maxres = max(maxres, left+right);
       return max(left, right) + 1;
    }
};