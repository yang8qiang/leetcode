/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年10月01日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include "main.h"

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int sum = 0;

    void converttoBST(TreeNode* root){
        if(!root)
            return;
        
        converttoBST(root->right);

        root->val +=sum;
        sum = root->val;

        converttoBST(root->left);
    }


public:
    TreeNode* convertBST(TreeNode* root) {
        if( !root )
            return NULL;
        
        converttoBST(root);
        return root;
    }
};