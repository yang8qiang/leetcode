/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年05月19日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    int lastRemaining(int n) {
        int current_head = 1;
        int turn = 1;
        while(n != 1){
            if( turn%2 == 0  ){
                if( n%2 != 0 ){
                    current_head += pow(2, turn-1);
                }
            }else{
                current_head += pow(2, turn-1);
            }
            
            turn += 1;
            n=n/2;
        }

        return current_head;
    }
};


class Solution {
public:
    int lastRemaining(int n) {
        int current_head = 1;
        int turn = 1;
        while(n != 1){
            if(turn%2 == 1 || n%2 != 0){
                current_head += pow(2, turn-1);
            }

            turn += 1;
            n=n/2;
        }

        return current_head;
    }
};
