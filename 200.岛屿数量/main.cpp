/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年05月26日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int numIslands(vector<vector<char>>& grid) {
        int m = grid.size();
        if( m == 0 )
            return 0;
        int n = grid[0].size(), res = 0;
        
        for(int i = 0; i < m; i++)
            for(int j = 0; j < n; j++)
                if(grid[i][j] == '1'){
                    res++;
                    find(grid,i,j);
                }

        return res;
    }
    
    void find(vector<vector<char>> &grid, int start, int end){
        if(start < 0 || start == grid.size() || end < 0 || end == grid[0].size() || grid[start][end] == '0')
            return;

        grid[start][end] = '0';
        find(grid,start-1,end);
        find(grid,start+1,end);
        find(grid,start,end-1);
        find(grid,start,end+1);
    }
};
