class Solution(object):
    def findMinDifference(self, timePoints):
        """
        :type timePoints: List[str]
        :rtype: int
        """
        length = len(timePoints)
        if length <= 1:
            return 0

        times = list()
        axist = dict()
        for time in timePoints:
            result = self.convert_time(time)
            if not axist.get(result):
                times.append(result)
                axist[result] = True
            else:
                return 0

        times.sort()
        minres = 24*60 + 1
        for i in range(1, length):
            minres = min(minres, times[i] - times[i-1])

        minres = min(minres, 24*60 + times[0] - times[len(times) - 1])

        return minres

    def convert_time(self, time):
        hour = int(time[0:2])
        minute = int(time[3:5])

        return hour*60+minute
