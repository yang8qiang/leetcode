/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年07月20日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*
================================================================*/


#include <iostream>
#include <vector>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int maxres = INT_MIN;
    int maxPathSum(TreeNode* root) {
        dfs(root);

        return maxres;
    }

private:
    int dfs(TreeNode *root){
        if(!root){
            return 0;
        }

        int leftmax = max(0, dfs(root->left));
        int rightmax = max(0, dfs(root->right));

        maxres = max(maxres, (rightmax + root->val + leftmax));
        return max(leftmax+root->val, rightmax+root->val);
    }
};
