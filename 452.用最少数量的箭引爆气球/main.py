#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年01月04日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution:
    def __init__(self):
        self.min_shots = 0

    def findMinArrowShots(self, points) -> int:
        if len(points) == 0:
            return 0
        points.sort(key=lambda x: x[1])
        last_point = points[0]
        self.min_shots = 1
        for point in points:
            if point[0] > last_point[1]:
                self.min_shots += 1
                last_point = point

        return self.min_shots
