/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年11月04日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

class Solution {
public:
    int uniquePaths(int m, int n) {
        if(n <=0 || m <= 0)
            return 0;
        vector<vector<int> > path(m, vector<int> (n, 1));
        for(int i = 1; i < m; i++){
            for(int j = 1; j < n; j++)
                path[i][j] = path[i-1][j] + path[i][j-1];    
        }
        
        return path[m-1][n-1];
    }
};
