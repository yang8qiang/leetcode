/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年01月06日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int findMin(vector<int>& nums) {
        int start = 0, end = nums.size() - 1;
        int mid = 0;
        while( start < end ){
            mid = start + (end-start)/2;
            if( nums[mid] < nums[end] )
                end = mid;
            else if( nums[mid] > nums[end] )
                start = mid + 1;
            else
                end--;
        }

        return nums[start];
    }

};

