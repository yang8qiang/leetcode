/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年06月17日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <stack>

using namespace std;

class MinStack {
public:
    /** initialize your data structure here. */
    stack<int> data_stack;
    stack<int> min_stack;   

    MinStack() {
    }
    
    void push(int x) {
        data_stack.push(x);
        if(min_stack.empty() || x <= min_stack.top())
            min_stack.push(x);
    }
    
    void pop() {
        if( data_stack.empty() )
            return;

        if( data_stack.top() == min_stack.top())
            min_stack.pop();

        data_stack.pop();
    }
    
    int top() {
        return data_stack.top();
    }
    
    int getMin() {
        return min_stack.top();
    }
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(x);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->getMin();
 */
