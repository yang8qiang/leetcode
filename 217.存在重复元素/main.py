#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年06月30日
描    述：
Copyright (C) 2019 All rights reserved.
'''

class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        length = len(nums)
        if length == 0:
            return False

        map = {}
        for i in range(0, length):
            value = map.get(nums[i])
            if value == None:
                map[nums[i]] = 1
            else:
                return True

        return False
        
