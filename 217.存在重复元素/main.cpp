/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年06月30日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <map>
#include <vector>

using namespace std;

class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        int size = nums.size();
        if( size == 0 )
            return false;
        
        map<int, int> datamap;
        for( int i = 0; i < size; i++ ){
            if( datamap.count(nums[i]) != 0 ){
                return true;
            }else{
                datamap.insert(pair<int, int>(nums[i], 1));
            }
        }

        return false;
    }
};
