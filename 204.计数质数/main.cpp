/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年06月30日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class Solution {
public:
    int countPrimes(int n) {
        vector<bool>flag (n, false);
        int count = 0;
        
		for(int i = 2; i < n; i++){
            if( flag[i] == false ){
                count++;
                for( int j = 2; i*j < n; j++ ){
                    flag[i*j] = true;
                }
            } 
        }

        return count;
    }

};
