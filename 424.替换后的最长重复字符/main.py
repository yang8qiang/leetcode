#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年03月01日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''
class Solution(object):
    def characterReplacement(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: int
        """

        map = dict()
        length = len(s)
        i = k
        charmax = 0
        left = 0
        right = 0
        while right < length:
            value = map.get(s[right], 0)
            map[s[right]] = value + 1
            charmax = max(charmax, value+1)
            if right - left + 1 > charmax + k:
                if map.get(s[left]) > 0:
                    map[s[left]] -= 1
                right += 1
                left += 1
            else:
                right += 1
            

        return right - leftt

