/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年07月26日
*   描    述：
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        while( true ){
            if( root->val == p->val || root->val == q->val )
                return root;
            if( root->val > p->val && root->val > q->val )
                root = root->left;
            else if( root->val < p->val && root->val < q->val )
                root = root->right;
            else
                return root;
        } 
    }
};

