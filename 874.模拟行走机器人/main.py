#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年03月18日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def __init__(self):
        self.dx = [1, 0, -1, 0]
        self.dy = [0, 1, 0, -1]

    def robotSim(self, commands, obstacles):
        """
        :type commands: List[int]
        :type obstacles: List[List[int]]
        :rtype: int
        """

        csize = len(commands)
        osize = len(obstacles)
        if csize == 0:
            return 0

        dic = dict()
        obstacle = list()
        j = 0
        for i in range(osize):
            value = dic.get(obstacles[i][0], None)
            if not value:
                dic[obstacles[i][0]] = j
                obstacle.append([])
                obstacle[j].append(obstacles[i][1])
                j += 1
            else:
                obstacle[value].append(obstacles[i][1])

        di = 1
        x = ny = 0
        y = ny = 0
        res = 0
        for command in commands:
            if command == -2:
                di = (di+1) % 4
            elif command == -1:
                di = (di+3) % 4
            else:
                for _ in range(command):
                    nx = x + self.dx[di]
                    ny = y + self.dy[di]
                    index = dic.get(nx, -1)
                    if index >= 0:
                        flag = False
                        for tmp in obstacle[index]:
                            if tmp == ny:
                                flag = True
                                break
                        if flag:
                            break

                    x = nx
                    y = ny
                    res = max(res, x*x+y*y)

        return res