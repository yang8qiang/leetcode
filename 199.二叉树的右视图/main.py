#!/usr/bin/env python3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2018年12月15日
描    述：
Copyright (C) 2018 All rights reserved.
'''


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def rightSideView(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """

        if root == None:
            return []
        
        result = [1]
        tmp_queue = []
        tmp_queue.append(root)
        while len(tmp_queue):
            size = len(tmp_queue)
            while size > 0:
                if root.left:
                    tmp_queue.append(root.left)
                if root.right:
                    tmp_queue.append(root.right)
                p = tmp_queue[0]
                tmp_queue.pop(0)
                size = size - 1
            
            result.append(p)

        return result
            
            
            

