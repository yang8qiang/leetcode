/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年12月15日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>
#include <queue>
using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {
        vector<int> res;
        if( root == NULL ){
            return res;
        }

        TreeNode *p = NULL;
        int size = 0;

        queue<TreeNode *> q;
        q.push(root);
        while( !q.empty() ){
            size = q.size();
            while( size != 0 ){
                p = q.front();
                if( p -> left )
                    q.push(p->left);
                if( p->right )
                    q.push(p->right);

                q.pop();
                size--;
            }

            res.push_back(p->val);
        }

        return res;
    }
};