#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2019年08月11日
描    述：
'''


class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        if k <= 0:
            return

        length = len(nums)
        k = k % length
        step = length - k

        tmp = []
        j = 0
        for i in range(step):
            tmp.append(nums[i])
        for i in range(step, length):
            nums[j] = nums[i]
            j += 1
        for i in range(len(tmp)):
            nums[j] = tmp[i]
            j += 1

        return


class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        if k <= 0:
            return

        k = k % len(nums)
        self.reverse(nums, 0, len(nums) - 1)
        self.reverse(nums, 0, k - 1)
        self.reverse(nums, k, len(nums) - 1)

    def reverse(self, nums, start, end):
        while start < end:
            nums[start], nums[end] = nums[end], nums[start]
            start, end = start+1, end-1
