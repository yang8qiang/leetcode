/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年09月10日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <map>
#include <vector>
#include <stack>
using namespace std;

class Solution {
private:
    map<int, int>numsmap;
    vector<int> res;

public:
    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2) {
        int len2 = nums2.size(), len1 = nums1.size();
        if( len2 == 0 || len1 == 0 ){
            return res;
        }

        stack<int> s;
        int num = 0;
        for(int i = 0; i < len2; i++){
            num = nums2[i];
            while(!s.empty() && s.top() < num){
                numsmap[s.top()] = num;
                s.pop();
            }

            numsmap[num] = -1;
            s.push(num);
        }

        for(int i = 0; i < len1; i++){
            res.push_back(numsmap[nums1[i]]);
        }

        return res;
    }
};
