#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年05月18日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def dominantIndex(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        maxnum = max(nums)
        pos = -1
        
        for i in range(len(nums)):
            if maxnum == nums[i] and pos == -1:
                pos = i
                continue

            if maxnum < 2*nums[i] and maxnum != nums[i]:
                return -1

        return pos
        