/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年07月20日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>
#include <stack>
using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
		if( !headA || !headB )
            return NULL;

        ListNode *p1 = headA;
        ListNode *p2 = headB;
        while( p1 && p2 && p1 != p2 ){
            p1 = p1->next;
            p2 = p2->next;
            if( p1 == p2 )
                return p1;

            if( !p1 )
                p1 = headB;
            if( !p2 )
                p2 = headA;
        }

        return p1;
    }
};
