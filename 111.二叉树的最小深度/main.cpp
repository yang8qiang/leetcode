/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年07月18日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
#include <stack>
#include <queue>
using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int minDepth(TreeNode* root) {      
        if( !root )
            return 0;

        int mindeep = 0, len = 0;
        queue<TreeNode *> q;
        q.push(root);
        TreeNode *p = NULL;
        while( !q.empty() ){
            mindeep++;
            len = q.size();
            while( len-- ){
                p = q.front();
                q.pop();
                if( !p->left &&  !p->right ){
                    return mindeep;
                }

                if( p->left )
                    q.push(p->left);
                if( p->right )
                    q.push(p->right);
            }
        }
        
        return mindeep;
    }

};
