class Solution(object):
    def repeatedSubstringPattern(self, s):
        """
        :type s: str
        :rtype: bool
        """

        new_str = (s*2)[1:-1]
        return True if s in new_str else False
