/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年06月04日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */


class Solution {
public:
    bool isSymmetric(TreeNode* root) {
        if( root == NULL )
            return true;

        return isMirror(root->left, root->right);
    }

private:
    bool isMirror(TreeNode* p, TreeNode* q){
        if( p == NULL && q == NULL ){
            return true;
        }

        if( !p && q || p && !q ){
            return false;
        }
        
        if(p->val != q->val){
            return false;
        }
        
        return isMirror(p->left, q->right) && isMirror(p->right, q->left);
    }
};
