/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年08月11日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    vector<string> summaryRanges(vector<int>& nums) {
        vector<string> res;
        int size = nums.size();

        int step = 0;
        string start, end;
        for( int i = 0; i < size; i++){
            start = to_string(nums[i]);
            while( i + 1 < size && (nums[i+1] == nums[i] + 1) ){
                i++;
            }

            end = to_string(nums[i]);
            if(start == end){
                res.push_back(start);
            }else{
                res.push_back(start + "->" + end);
            }
        }
        
        return res;
    }
};