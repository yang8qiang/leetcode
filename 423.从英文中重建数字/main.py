#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

class Solution:
    def originalDigits(self, s: str) -> str:
        chars = dict()
        num = dict()

        for c in s:
            chars[c] = chars.get(c, 0) + 1

        num[0] = chars.get('z', 0)
        num[2] = chars.get('w', 0)
        num[4] = chars.get('u', 0)
        num[6] = chars.get('x', 0)
        num[8] = chars.get('g', 0)

        num[3] = chars.get('h', 0) - num[8]
        num[5] = chars.get('f', 0) - num[4]
        num[7] = chars.get('s', 0) - num[6]

        num[1] = chars.get('o', 0) - num[0] - num[2] - num[4]
        num[9] = chars.get('i', 0) - num[5] - num[6] - num[8]

        retstr = ""
        for i in range(0, 10):
            if num[i] > 0:
                retstr += str(i) * num[i]

        return retstr
