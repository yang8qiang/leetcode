/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年12月21日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    int repeatedStringMatch(string A, string B) {
        int sizea = A.size();
        int sizeb = B.size();
        
        for(int i = 0; i < sizea; i++){
            if(A[i] == B[0]){
                int k = i, count = 1, j = 0;
                while(A[k] == B[j]){
                    k++;
                    j++;
                    if(j >= sizeb){
                        return count;
                    }
                    if(k >= sizea){
                        count++;
                        k = 0;
                    }
                }
            }
        }

        return -1;
    }
};