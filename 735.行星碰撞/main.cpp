/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月11日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <cmath>
#include <vector>
#include <stack>

using namespace std;

class Solution {
public:
    vector<int> asteroidCollision(vector<int>& asteroids) {
        int size = asteroids.size();
        vector<int> res;

        int tmp = 0;
        bool flag = false;
        for(int i = 0; i < size; i++){
            flag =false;
            while(!res.empty() && (asteroids[i] < 0 && res[res.size() - 1] > 0) ){
                tmp = res[res.size() - 1];
                if(abs(asteroids[i]) > tmp){
                    res.pop_back();
                    continue;
                }else if(abs(asteroids[i]) == tmp){
                    res.pop_back();
                }

                flag = true;
                break;
            }

            if(flag == false){
                res.push_back(asteroids[i]);
            }
        }

        return res;
    }
};