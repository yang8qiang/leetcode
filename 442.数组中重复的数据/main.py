from typing import List

class Solution:
    def findDuplicates(self, nums: List[int]) -> List[int]:
        length = len(nums)
        for i in range(0, length):
            while nums[i] != nums[nums[i] - 1]:
                nums[nums[i] - 1], nums[i] = nums[i], nums[nums[i] - 1]

        res = list()
        for i in range(0, length):
            if nums[i] - 1 != i:
                res.append(nums[i])

        return res

if __name__ == "__main__":
    solution = Solution()
    print(solution.findDuplicates([2, 3, 6, 7, 4, 3, 2, 5]))
