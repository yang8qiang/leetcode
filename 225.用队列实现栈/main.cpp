/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2019年05月25日
*   描    述：
*   Copyright (C) 2019 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <queue>

using namespace std;

class MyStack {
public:
    queue<int> q;
    
    /** Initialize your data structure here. */
    MyStack() {
    }
        
        /** Push element x onto stack. */
    void push(int x) {
        int size = q.size();
        q.push(x);
        for(int i = 0; i < size; i++){
            q.push(q.front());
            q.pop();
        }
    }
        
        /** Removes the element on top of the stack and returns that element. */
    int pop() {
        int res = q.front();
        q.pop();
        return res;
    }
        
        /** Get the top element. */
    int top() {
        return q.front();
    }
        
        /** Returns whether the stack is empty. */
    bool empty() {
        return q.empty();
    }
    
};

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack* obj = new MyStack();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->top();
 * bool param_4 = obj->empty();
 */
