#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年03月05日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution(object):
    def findRestaurant(self, list1, list2):
        """
        :type list1: List[str]
        :type list2: List[str]
        :rtype: List[str]
        """

        dic = dict()
        res = list()
        min = len(list1) + len(list2) - 1
        for i in range(len(list1)):
            dic[list1[i]] = i

        for i in range(len(list2)):
            pos = dic.get(list2[i], -1)
            if pos >= 0 and min >= i + pos:
                if min > i + pos:
                    min = i + pos
                    res = []
                res.append(list2[i])

        return res


if __name__ == "__main__":
    s1 = ["dsdsadgd", "ewre", "dddeg", "dsafegg", "sfdaefasd", "sgdsgdsg", "sfdagegrhfaswr"]
    s2 = ["sfsdafds", "dddeg", "ssfsdafdsfg", "dsdsadgd", "sfdsgdsgdsgrhre", "sfdaefasd"]

    a = Solution()
    print(a.findRestaurant(s1, s2))
